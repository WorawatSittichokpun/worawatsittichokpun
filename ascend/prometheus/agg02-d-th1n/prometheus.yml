global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  scrape_timeout:      15s # is set to the global default (10s).

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - '10.14.24.111:9093'

rule_files:
  - "./rules/infra/*.yml"
  - "./rules/app/*.yml"

scrape_configs:
  - job_name: 'node-exporter'
    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 10s
    dns_sd_configs:
    - names:
      - 'tasks.node-exporter'
      type: 'A'
      port: 9100
    static_configs:
    - targets: ['node-exporter:9100']

  - job_name: 'federate_kube_psre'
    scheme: https
    scrape_interval: 30s
    scrape_timeout: 30s
    tls_config:
      insecure_skip_verify: true
    honor_labels: true
    relabel_configs:
    - replacement: "openshift"
      target_label: "instance_group"
    metrics_path: '/federate'
    params:
      'match[]':
        - '{__name__=~".+"}'
    metric_relabel_configs:
     - source_labels: [container]
       target_label: container_name
       action: replace
       replacement: ${1}
     - source_labels: [namespace]
       regex: .*-(.*)
       target_label: env
       replacement: ${1}
       action: replace
    static_configs:
      - targets:
        - 'prometheus-psre.general-d-th1n.ascendmoney.io'

  - job_name: haproxy_exporter_general
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
    - files:
      - /etc/prometheus/oc_route_general.json
      refresh_interval: 5m
    basic_auth:
      username: dXNlcm53bW01
      password: cGFzc2R3NnM2
    relabel_configs:
    - separator: ;
      regex: (.*)
      target_label: route_group
      replacement: genereal
      action: replace

  - job_name: haproxy_exporter_private
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
    - files:
      - /etc/prometheus/oc_route_private.json
      refresh_interval: 5m
    basic_auth:
      username: dXNlcmpidDUy
      password: cGFzc2Z0dHJ4
    relabel_configs:
    - separator: ;
      regex: (.*)
      target_label: route_group
      replacement: private
      action: replace

  - job_name: haproxy_exporter_partner
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
    - files:
      - /etc/prometheus/oc_route_partner.json
      refresh_interval: 5m
    basic_auth:
      username: dXNlcndmendk
      password: cGFzczV3MnBm
    relabel_configs:
    - separator: ;
      regex: (.*)
      target_label: route_group
      replacement: partner
      action: replace

  - job_name: haproxy_exporter_public
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
    - files:
      - /etc/prometheus/oc_route_public.json
      refresh_interval: 5m
    basic_auth:
      username: dXNlcmx6d2J3
      password: cGFzczJzcHo5
    relabel_configs:
    - separator: ;
      regex: (.*)
      target_label: route_group
      replacement: public
      action: replace

  - job_name: haproxy_exporter_backoffice
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
    - files:
      - /etc/prometheus/oc_route_backoffice.json
      refresh_interval: 5m
    basic_auth:
      username: dXNlcnFza3M0
      password: cGFzczRzazd0
    relabel_configs:
    - separator: ;
      regex: (.*)
      target_label: route_group
      replacement: backoffice
      action: replace

  - job_name: haproxy_exporter_cicd
    honor_timestamps: true
    scrape_interval: 10s
    scrape_timeout: 10s
    metrics_path: /metrics
    scheme: http
    file_sd_configs:
    - files:
      - /etc/prometheus/oc_route_cicd.json
      refresh_interval: 5m
    basic_auth:
      username: dXNlcjg0cXhi
      password: cGFzc3hjNjZx
    relabel_configs:
    - separator: ;
      regex: (.*)
      target_label: route_group
      replacement: cicd
      action: replace