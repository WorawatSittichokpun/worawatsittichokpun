# Converted with pg2mysql-1.9
# Converted on Tue, 30 Mar 2021 06:07:42 -0400
# Lightbox Technologies Inc. http://www.lightbox.ca

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone="+00:00";

CREATE DATABASE `grafana` DEFAULT CHARACTER SET UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8;

CREATE TABLE public.alert (
    id int(11) NOT NULL,
    version bigint NOT NULL,
    dashboard_id bigint NOT NULL,
    panel_id bigint NOT NULL,
    org_id bigint NOT NULL,
    name varchar(255) NOT NULL,
    message text NOT NULL,
    state varchar(190) NOT NULL,
    settings text NOT NULL,
    frequency bigint NOT NULL,
    handler bigint NOT NULL,
    severity text NOT NULL,
    silenced bool NOT NULL,
    execution_error text NOT NULL,
    eval_data text,
    eval_date timestamp,
    new_state_date timestamp NOT NULL,
    state_changes int(11) NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    `for` bigint
) TYPE=MyISAM;

CREATE TABLE public.alert_notification (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    name varchar(190) NOT NULL,
    `type` varchar(255) NOT NULL,
    settings text NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    is_default bool DEFAULT 0 NOT NULL,
    frequency bigint,
    send_reminder bool,
    disable_resolve_message bool NOT NULL,
    uid varchar(40),
    secure_settings text
) TYPE=MyISAM;

CREATE TABLE public.alert_notification_state (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    alert_id bigint NOT NULL,
    notifier_id bigint NOT NULL,
    state varchar(50) NOT NULL,
    version bigint NOT NULL,
    updated_at bigint NOT NULL,
    alert_rule_state_updated_version bigint NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.alert_rule_tag (
    alert_id bigint NOT NULL,
    tag_id bigint NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.annotation (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    alert_id bigint,
    user_id bigint,
    dashboard_id bigint,
    panel_id bigint,
    category_id bigint,
    `type` varchar(25) NOT NULL,
    title text NOT NULL,
    text text NOT NULL,
    metric varchar(255),
    prev_state varchar(25) NOT NULL,
    new_state varchar(25) NOT NULL,
    data text NOT NULL,
    epoch bigint NOT NULL,
    region_id bigint DEFAULT 0,
    tags text,
    created bigint DEFAULT 0,
    updated bigint DEFAULT 0,
    epoch_end bigint DEFAULT 0 NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.annotation_tag (
    annotation_id bigint NOT NULL,
    tag_id bigint NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.api_key (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    name varchar(190) NOT NULL,
    key varchar(190) NOT NULL,
    role varchar(255) NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    expires bigint
) TYPE=MyISAM;

CREATE TABLE public.cache_data (
    cache_key varchar(168) NOT NULL,
    data BLOB NOT NULL,
    expires int(11) NOT NULL,
    created_at int(11) NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.dashboard (
    id int(11) NOT NULL,
    version int(11) NOT NULL,
    slug varchar(189) NOT NULL,
    title varchar(189) NOT NULL,
    data text NOT NULL,
    org_id bigint NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    updated_by int(11),
    created_by int(11),
    gnet_id bigint,
    plugin_id varchar(189),
    folder_id bigint DEFAULT 0 NOT NULL,
    is_folder bool NOT NULL,
    has_acl bool NOT NULL,
    uid varchar(40)
) TYPE=MyISAM;

CREATE TABLE public.dashboard_acl (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    dashboard_id bigint NOT NULL,
    user_id bigint,
    team_id bigint,
    permission smallint DEFAULT 4 NOT NULL,
    role varchar(20),
    created timestamp NOT NULL,
    updated timestamp NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.dashboard_provisioning (
    id int(11) NOT NULL,
    dashboard_id bigint,
    name varchar(150) NOT NULL,
    external_id text NOT NULL,
    updated int(11) DEFAULT 0 NOT NULL,
    check_sum varchar(32)
) TYPE=MyISAM;

CREATE TABLE public.dashboard_snapshot (
    id int(11) NOT NULL,
    name varchar(255) NOT NULL,
    key varchar(190) NOT NULL,
    delete_key varchar(190) NOT NULL,
    org_id bigint NOT NULL,
    user_id bigint NOT NULL,
    external bool NOT NULL,
    external_url varchar(255) NOT NULL,
    dashboard text NOT NULL,
    expires timestamp NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    external_delete_url varchar(255),
    dashboard_encrypted BLOB
) TYPE=MyISAM;

CREATE TABLE public.dashboard_tag (
    id int(11) NOT NULL,
    dashboard_id bigint NOT NULL,
    term varchar(50) NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.dashboard_version (
    id int(11) NOT NULL,
    dashboard_id bigint NOT NULL,
    parent_version int(11) NOT NULL,
    restored_from int(11) NOT NULL,
    version int(11) NOT NULL,
    created timestamp NOT NULL,
    created_by bigint NOT NULL,
    message text NOT NULL,
    data text NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.data_source (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    version int(11) NOT NULL,
    `type` varchar(255) NOT NULL,
    name varchar(190) NOT NULL,
    access varchar(255) NOT NULL,
    url varchar(255) NOT NULL,
    password varchar(255),
    `user` varchar(255),
    database varchar(255),
    basic_auth bool NOT NULL,
    basic_auth_user varchar(255),
    basic_auth_password varchar(255),
    is_default bool NOT NULL,
    json_data text,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    with_credentials bool DEFAULT 0 NOT NULL,
    secure_json_data text,
    read_only bool,
    uid varchar(40) DEFAULT 0 NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.login_attempt (
    id int(11) NOT NULL,
    username varchar(190) NOT NULL,
    ip_address varchar(30) NOT NULL,
    created int(11) DEFAULT 0 NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.migration_log (
    id int(11) NOT NULL,
    migration_id varchar(255) NOT NULL,
    sql text NOT NULL,
    success bool NOT NULL,
    error text NOT NULL,
    `timestamp` timestamp NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.org (
    id int(11) NOT NULL,
    version int(11) NOT NULL,
    name varchar(190) NOT NULL,
    address1 varchar(255),
    address2 varchar(255),
    city varchar(255),
    state varchar(255),
    zip_code varchar(50),
    country varchar(255),
    billing_email varchar(255),
    created timestamp NOT NULL,
    updated timestamp NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.org_user (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    user_id bigint NOT NULL,
    role varchar(20) NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.playlist (
    id int(11) NOT NULL,
    name varchar(255) NOT NULL,
    `interval` varchar(255) NOT NULL,
    org_id bigint NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.playlist_item (
    id int(11) NOT NULL,
    playlist_id bigint NOT NULL,
    `type` varchar(255) NOT NULL,
    value text NOT NULL,
    title text NOT NULL,
    `order` int(11) NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.plugin_setting (
    id int(11) NOT NULL,
    org_id bigint,
    plugin_id varchar(190) NOT NULL,
    enabled bool NOT NULL,
    pinned bool NOT NULL,
    json_data text,
    secure_json_data text,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    plugin_version varchar(50)
) TYPE=MyISAM;

CREATE TABLE public.preferences (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    user_id bigint NOT NULL,
    version int(11) NOT NULL,
    home_dashboard_id bigint NOT NULL,
    timezone varchar(50) NOT NULL,
    theme varchar(20) NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    team_id bigint
) TYPE=MyISAM;

CREATE TABLE public.quota (
    id int(11) NOT NULL,
    org_id bigint,
    user_id bigint,
    target varchar(190) NOT NULL,
    `limit` bigint NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.server_lock (
    id int(11) NOT NULL,
    operation_uid varchar(100) NOT NULL,
    version bigint NOT NULL,
    last_execution bigint NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.session (
    key varchar(16) NOT NULL,
    data BLOB NOT NULL,
    expiry int(11) NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.short_url (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    uid varchar(40) NOT NULL,
    path text NOT NULL,
    created_by int(11) NOT NULL,
    created_at int(11) NOT NULL,
    last_seen_at int(11)
) TYPE=MyISAM;

CREATE TABLE public.star (
    id int(11) NOT NULL,
    user_id bigint NOT NULL,
    dashboard_id bigint NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.tag (
    id int(11) NOT NULL,
    key varchar(100) NOT NULL,
    value varchar(100) NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.team (
    id int(11) NOT NULL,
    name varchar(190) NOT NULL,
    org_id bigint NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    email varchar(190)
) TYPE=MyISAM;

CREATE TABLE public.team_member (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    team_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    external bool,
    permission smallint
) TYPE=MyISAM;

CREATE TABLE public.temp_user (
    id int(11) NOT NULL,
    org_id bigint NOT NULL,
    version int(11) NOT NULL,
    email varchar(190) NOT NULL,
    name varchar(255),
    role varchar(20),
    code varchar(190) NOT NULL,
    `status` varchar(20) NOT NULL,
    invited_by_user_id bigint,
    email_sent bool NOT NULL,
    email_sent_on timestamp,
    remote_addr varchar(255),
    created int(11) DEFAULT 0 NOT NULL,
    updated int(11) DEFAULT 0 NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.test_data (
    id int(11) NOT NULL,
    metric1 varchar(20),
    metric2 varchar(150),
    value_big_int bigint,
    value_double double precision,
    value_float real,
    value_int int(11),
    time_epoch bigint NOT NULL,
    time_date_time timestamp NOT NULL,
    time_time_stamp timestamp NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.`user` (
    id int(11) NOT NULL,
    version int(11) NOT NULL,
    login varchar(190) NOT NULL,
    email varchar(190) NOT NULL,
    name varchar(255),
    password varchar(255),
    salt varchar(50),
    rands varchar(50),
    company varchar(255),
    org_id bigint NOT NULL,
    is_admin bool NOT NULL,
    email_verified bool,
    theme varchar(255),
    created timestamp NOT NULL,
    updated timestamp NOT NULL,
    help_flags1 bigint DEFAULT 0 NOT NULL,
    last_seen_at timestamp,
    is_disabled bool DEFAULT 0 NOT NULL
) TYPE=MyISAM;

CREATE TABLE public.user_auth (
    id int(11) NOT NULL,
    user_id bigint NOT NULL,
    auth_module varchar(190) NOT NULL,
    auth_id varchar(190) NOT NULL,
    created timestamp NOT NULL,
    o_auth_access_token text,
    o_auth_refresh_token text,
    o_auth_token_type text,
    o_auth_expiry timestamp
) TYPE=MyISAM;

CREATE TABLE public.user_auth_token (
    id int(11) NOT NULL,
    user_id bigint NOT NULL,
    auth_token varchar(100) NOT NULL,
    prev_auth_token varchar(100) NOT NULL,
    user_agent varchar(255) NOT NULL,
    client_ip varchar(255) NOT NULL,
    auth_token_seen bool NOT NULL,
    seen_at int(11),
    rotated_at int(11) NOT NULL,
    created_at int(11) NOT NULL,
    updated_at int(11) NOT NULL
) TYPE=MyISAM;

ALTER TABLE public.alert_notification
    ADD CONSTRAINT alert_notification_pkey PRIMARY KEY (id);
ALTER TABLE public.alert_notification_state
    ADD CONSTRAINT alert_notification_state_pkey PRIMARY KEY (id);
ALTER TABLE public.alert
    ADD CONSTRAINT alert_pkey PRIMARY KEY (id);
ALTER TABLE public.annotation
    ADD CONSTRAINT annotation_pkey PRIMARY KEY (id);
ALTER TABLE public.api_key
    ADD CONSTRAINT api_key_pkey1 PRIMARY KEY (id);
ALTER TABLE public.cache_data
    ADD CONSTRAINT cache_data_pkey PRIMARY KEY (cache_key);
ALTER TABLE public.dashboard_acl
    ADD CONSTRAINT dashboard_acl_pkey PRIMARY KEY (id);
ALTER TABLE public.dashboard
    ADD CONSTRAINT dashboard_pkey1 PRIMARY KEY (id);
ALTER TABLE public.dashboard_provisioning
    ADD CONSTRAINT dashboard_provisioning_pkey1 PRIMARY KEY (id);
ALTER TABLE public.dashboard_snapshot
    ADD CONSTRAINT dashboard_snapshot_pkey PRIMARY KEY (id);
ALTER TABLE public.dashboard_tag
    ADD CONSTRAINT dashboard_tag_pkey PRIMARY KEY (id);
ALTER TABLE public.dashboard_version
    ADD CONSTRAINT dashboard_version_pkey PRIMARY KEY (id);
ALTER TABLE public.data_source
    ADD CONSTRAINT data_source_pkey1 PRIMARY KEY (id);
ALTER TABLE public.login_attempt
    ADD CONSTRAINT login_attempt_pkey1 PRIMARY KEY (id);
ALTER TABLE public.migration_log
    ADD CONSTRAINT migration_log_pkey PRIMARY KEY (id);
ALTER TABLE public.org
    ADD CONSTRAINT org_pkey PRIMARY KEY (id);
ALTER TABLE public.org_user
    ADD CONSTRAINT org_user_pkey PRIMARY KEY (id);
ALTER TABLE public.playlist_item
    ADD CONSTRAINT playlist_item_pkey PRIMARY KEY (id);
ALTER TABLE public.playlist
    ADD CONSTRAINT playlist_pkey PRIMARY KEY (id);
ALTER TABLE public.plugin_setting
    ADD CONSTRAINT plugin_setting_pkey PRIMARY KEY (id);
ALTER TABLE public.preferences
    ADD CONSTRAINT preferences_pkey PRIMARY KEY (id);
ALTER TABLE public.quota
    ADD CONSTRAINT quota_pkey PRIMARY KEY (id);
ALTER TABLE public.server_lock
    ADD CONSTRAINT server_lock_pkey PRIMARY KEY (id);
ALTER TABLE public.session
    ADD CONSTRAINT session_pkey PRIMARY KEY (key);
ALTER TABLE public.short_url
    ADD CONSTRAINT short_url_pkey PRIMARY KEY (id);
ALTER TABLE public.star
    ADD CONSTRAINT star_pkey PRIMARY KEY (id);
ALTER TABLE public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);
ALTER TABLE public.team_member
    ADD CONSTRAINT team_member_pkey PRIMARY KEY (id);
ALTER TABLE public.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);
ALTER TABLE public.temp_user
    ADD CONSTRAINT temp_user_pkey1 PRIMARY KEY (id);
ALTER TABLE public.test_data
    ADD CONSTRAINT test_data_pkey PRIMARY KEY (id);
ALTER TABLE public.user_auth
    ADD CONSTRAINT user_auth_pkey PRIMARY KEY (id);
ALTER TABLE public.user_auth_token
    ADD CONSTRAINT user_auth_token_pkey PRIMARY KEY (id);
ALTER TABLE public.`user`
    ADD CONSTRAINT user_pkey1 PRIMARY KEY (id);
