#!/usr/bin/env python

import json
import os
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


GF_SRC_HOST = "https://mp-grafana01.ascendmoney.internal"
#API KEY clone-data:view:never
GF_SRC_API_KEY = "eyJrIjoiWlk5NGN1OVpLYkV0cHQxbVlCUVJnMW1hak5nQzZyV3MiLCJuIjoiY2xvbmUtZGF0YSIsImlkIjozfQ=="

GF_DST_HOST = "https://bp-grafana01.ascendmoney.internal"
#API KEY clone-data:admin:never
GF_DST_API_KEY = "eyJrIjoiQTBrYjlLeXF2M0luWWNIY29yb1YyQklkQnBJY1FhMVoiLCJuIjoiY2xvbmUtZGF0YSIsImlkIjozfQ=="

HEADER_SRC = {"Authorization": f"Bearer {GF_SRC_API_KEY}"}
HEADER_DST = {"Authorization": f"Bearer {GF_DST_API_KEY}"}



def clone_folder():
    response = requests.get(
        f"{GF_SRC_HOST}/api/folders", headers=HEADER_SRC, verify=False
    )
    response.raise_for_status()
    folders = response.json()
    for f in folders:
        folder_title = f["title"]
        print(f"Cloning folder: {folder_title}")
        payload = {"title": folder_title}
        response = requests.post(
            url=f"{GF_DST_HOST}/api/folders",
            headers=HEADER_DST,
            data=payload,
            verify=False,
        )
        print(response.json())
        res = response.json()
        if response.status_code == 200:
            print(f"{folder_title}: {res['uid']}")
        else:
            print(f"{folder_title}: {res['message']}")


def get_folder_id(folder_name):
    response = requests.get(
        url=f"{GF_DST_HOST}/api/search?type=dash-folder&query={folder_name}",
        headers=HEADER_DST,
        verify=False,
    )
    res = response.json()[0]
    if response.status_code == 200:
        return res["id"]


def clone_dashboard():
    headers = {"Authorization": f"Bearer {GF_SRC_API_KEY}"}
    response = requests.get(
        f"{GF_SRC_HOST}/api/search?type=dash-db&query=&", headers=headers, verify=False
    )
    response.raise_for_status()
    dashboards = response.json()
    for d in dashboards:
        print("Cloning dashboard: " + d["title"])
        response = requests.get(
            f"{GF_SRC_HOST}/api/dashboards/{d['uri']}", headers=headers, verify=False,
        )
        dashboard = response.json()["dashboard"]
        dashboard["uid"] = None
        dashboard["id"] = None

        if "folderTitle" not in d:
            payload = {"dashboard": dashboard, "overwrite": False}
        else:
            folder_title = d["folderTitle"]
            folder_id = get_folder_id(folder_title)
            payload = {
                "dashboard": dashboard,
                "folderId": folder_id,
                "overwrite": False,
            }

        response = requests.post(
            url=f"{GF_DST_HOST}/api/dashboards/db",
            headers={
                "Authorization": f"Bearer {GF_DST_API_KEY}",
                "Content-type": "application/json;charset=UTF-8",
            },
            data=json.dumps(payload),
            verify=False,
        )
        print(response.content)


def clone_datasource():
    response = requests.get(
        f"{GF_SRC_HOST}/api/datasources", headers=HEADER_SRC, verify=False
    )
    response.raise_for_status()
    datasources = response.json()

    for d in datasources:
        #d['id'] = None
        #d['uid'] = None
        payload = d

        response = requests.post(
            url=f"{GF_DST_HOST}/api/datasources",
            headers={
                "Authorization": f"Bearer {GF_DST_API_KEY}",
                "Content-type": "application/json;charset=UTF-8",
            },
            data=json.dumps(payload),
            verify=False,
        )
        print(response.content)

def clone_snapshot():
    response = requests.get(
        f"{GF_SRC_HOST}/api/dashboard/snapshots", headers=HEADER_SRC, verify=False
    )
    response.raise_for_status()
    snapshot_metadatas = response.json()

    for snapshot_metadata in snapshot_metadatas:
        snapshot_key = snapshot_metadata['key']

        response = requests.get(
            f"{GF_SRC_HOST}/api/snapshots/{snapshot_key}", headers=HEADER_SRC, verify=False
        )
        response.raise_for_status()
        payload = response.json()['dashboard']

        try:
            del payload['annotations']
        except:
            print('||||| key does not exist |||||')
            
        #print(response.json()['meta']['folderId'])

        payload['id'] = None

        response = requests.post(
            url=f"{GF_DST_HOST}/api/snapshots",
            headers={
                "Authorization": f"Bearer {GF_DST_API_KEY}",
                "Content-type": "application/json;charset=UTF-8",
            },
            data=json.dumps(payload),
            verify=False,
        )
        print(response.content)


def main():
    clone_folder()
    clone_dashboard()
    #clone_datasource()

    # FUNCTION IS NOT WORKING BECAUSE RELATIONSHIP TO DASHBOARD
    #clone_snapshot()




if __name__ == "__main__":
    main()

