#!/usr/bin/env python

import json
import os
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# grafana host
GF_SRC_HOST = "https://mn-grafana01.ascendmoney-dev.internal"
#API KEY clone-data:view:never
GF_SRC_API_KEY = "eyJrIjoiTmtuRmk4d1JqZVc2bnk5UkxGY1BjUlJ2TEsyUlhIdzkiLCJuIjoiY2xvbmUtZGF0YSIsImlkIjoxfQ=="
GF_DST_HOST = "http://localhost:3001"
#API KEY clone-data:admin:never
GF_DST_API_KEY = "eyJrIjoiOTdWRHBaZ1VTMUE3MXNXY0o3d3V4WkpwS1N2N1Vzd3IiLCJuIjoiY2xvbmUtZGF0YSIsImlkIjoxfQ=="
HEADER_SRC = {"Authorization": f"Bearer {GF_SRC_API_KEY}"}
HEADER_DST = {"Authorization": f"Bearer {GF_DST_API_KEY}"}


def main():
    folder_id = get_folder_id("trying")
    print(folder_id)


if __name__ == "__main__":
    main()

